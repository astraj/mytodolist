//модель сама
//var todoModel = require('./models/todo');
todoManager = require('../models/todoManager');


// http://stackoverflow.com/questions/630453/put-vs-post-in-rest
//В Express маршруты оборачивают в функцию, 
//которая принимает экземпляр Express и базу данных как аргументы. 
module.exports = function(app) {
//-------------------------------------------------------------------------------------------------------
	// todo API
	// Получить список всех дел
	app.get('/todos', (req, res) => {
		todoManager.getItems(function(err, todos) {
			if (err)
				// TODO: err to output
				console.log(err);
			else
				res.json({
						items: todos
					})
		});
	});

	// создаем новое дело и возвращаем список всех дел
	app.put('/todos', (req, res) => {
		todoManager.addItem(req.body, function(err) {
			if (err)
				// TODO: err to output
				console.log(err)
			else
				res.json({
						"code" : 200,
						"msg": "ok"
					})}
		);
	});

	// удаление дела
	app.delete('/todos/:todo_id', (req, res) => {
		todoManager.deleteItem(req.params.todo_id, function(err) {
			if (err)
				// TODO: err to output
				console.log(err)
			else
				res.json({
						"code" : 200,
						"msg": "ok"
					})}
		);
	});

	// редактирование дела
	app.post('/todos/:todo_id', (req, res) => {
		todoManager.updateItem(req.params.todo_id, req.body , function(err) {
			if (err)
				// TODO: err to output
				console.log(err)
			else
				res.json({
						"code" : 200,
						"msg": "ok"
					})}
		);
	});
	// application -------------------------------------------------------------
	//получение страницы самого frontend'а
	app.get('*', function(req, res) {
		res.sendfile('./public/index.html'); // 
	});
};