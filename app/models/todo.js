var mongoose = require('mongoose');


var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

module.exports = mongoose.model('todo', {
	_id : ObjectId,
	text : String,
	//title : String,
	done : Boolean,
	date : Date
});