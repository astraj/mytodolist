var todoModel = require('./todo');

module.exports = {
	getItems: function(callback){
		todoModel.find(function(err, todos) {
                callback(err, todos);
			});
	},
	
	addItem: function(pointToSave, callback){
		todoModel.create({ 
			text: pointToSave.text, 
			//title: pointToSave.title ,
			done: false,
			date: pointToSave.date}
		,(err) => {
      		callback(err);
    	});
	},	

    deleteItem: function(pointToDelete, callback){
        todoModel.findByIdAndRemove(pointToDelete         
		, function(err){
			callback(err);
		});
    }, 

    updateItem: function(itemId, itemPar, callback){

        todoModel.findByIdAndUpdate(itemId, 			
            {text: itemPar.text, 
			//title: itemPar.title,
			done: itemPar.done,
			date: itemPar.date}
		    , function(err){
			callback(err);
		});
    }
}

