var express = require('express');

//var logger = require('morgan');
//вспомогательноге средство для работы с JSON, используем пакет body-parser
var bodyParser = require('body-parser');
//Позволяет использовать HTTP-команды, такие как PUT или DELETE, 
//в тех местах, где клиент их не поддерживает.
var methodOverride = require('method-override');

module.exports = function(app, config) {
  var env = process.env.NODE_ENV || 'development';
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';
  
//  app.set('views', config.root + '/app/views');
//  app.set('view engine', 'ejs');

  //app.use(logger('dev'));
  
  app.use(bodyParser.json());
  app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
// К сожалению, Express не может самостоятельно обрабатывать формы в URL-кодировке. 
// Тут нам на помощь придёт ранее установленный пакет body-parser.
  app.use(bodyParser.urlencoded({'extended':'true'}));

  app.use(express.static(__dirname + '/public')); 
  app.use(methodOverride());

//  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  //controllers.forEach(function (controller) {
    //require(controller)(app);
  //});

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  
  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        title: 'error'
      });
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
      });
  });

};