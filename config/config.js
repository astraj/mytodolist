var path = require('path');
var rootPath = path.normalize(__dirname + '/..');
var env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'qwe'
      },
    port: 8000,
    db: 'mongodb://ann:111@ds155150.mlab.com:55150/todo_db'
  }
};
  
module.exports = config[env];