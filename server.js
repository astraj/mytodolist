// set up ======================================================================
var express  = require('express');
var app      = express(); 		// инициализируем приложение экземпляром express
var mongoose = require('mongoose');
var bodyParser = require('body-parser'); 	//вспомогательноге средство для работы с JSON, используем пакет body-parser
const db = require('./config/db.js'); // url БД

var port  	 = 8000; //выбираем порт				

//Позволяет использовать HTTP-команды, такие как PUT или DELETE, 
//в тех местах, где клиент их не поддерживает.
var methodOverride = require('method-override'); // 



app.use(express.static(__dirname + '/public')); 				// set the static files location /public/img will be /img for users

// К сожалению, Express не может самостоятельно обрабатывать формы в URL-кодировке. 
// Тут нам на помощь придёт ранее установленный пакет body-parser.
app.use(bodyParser.urlencoded({'extended':'true'})); 			// parse application/x-www-form-urlencoded

// подключение к БД
mongoose.connect(db.url);

app.use(bodyParser.json()); 									// parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

app.set('view engine', 'ejs');

// routes ======================================================================
require('./app/controllers/routes.js')(app);

// listen (start app with node server.js) ======================================
app.listen(port, () => {
    console.log("App listening on port " + port);
});
