var angTodo = angular.module('fatalitaTodo', []);

function mainController($scope, $http) {
	$scope.formData = {};

	function updateData() {
		// получения списка всех дел для обновления
		$http.get('/todos')
			.success(function(data) {
				$scope.todos = data.items;
				$scope.todosNew = [];
				$scope.todosOverdue = [];
				$scope.todosReady = [];
				for (let i = 0; i < data.items.length; i++) {
					if(data.items[i].done == true)
						$scope.todosReady.push(data.items[i]);
					else if (new Date(data.items[i].date) > new Date())
						$scope.todosNew.push(data.items[i]);
					else $scope.todosOverdue.push(data.items[i]);
				}
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});		
	}	

	// получаем первый раз наш список
	updateData();

	//отправляем данные нашему api
	$scope.createTodo = function() {
		$http.put('/todos', $scope.formData)
			.success(function(data) {
				$scope.formData = {}; // clear the form so our user is ready to enter another
				updateData();
				//console.log(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};
	
	$scope.done= function() {
		for (var i = 0; i< $scope.todosNew.length; i++) {
			if($scope.todosNew[i].checked == true)
			{
				$scope.todosNew[i].done = true;
				$scope.editTodo($scope.todosNew[i]._id, $scope.todosNew[i]);
			}
		}
		updateData();
	};

	$scope.editTodo = function(id, info) {
		$http.post('/todos/' + id, info)
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	$scope.deleteList = function() {
		for (var i = 0; i< $scope.todosNew.length; i++) {
			if($scope.todosNew[i].checked == true)
			{
				//console.log($scope.todosNew[i]._id);
				$scope.deleteTodo($scope.todosNew[i]._id);
			}
		}
		for (var i = 0; i< $scope.todosOverdue.length; i++) {
			if($scope.todosOverdue[i].checked == true)
				$scope.deleteTodo($scope.todosOverdue[i]._id);
		}
		for (var i = 0; i< $scope.todosReady.length; i++) {
			if($scope.todosReady[i].checked == true)
				$scope.deleteTodo($scope.todosReady[i]._id);
		}
		updateData();
	};

	// удаляем дело
	$scope.deleteTodo = function(id) {
		console.log($scope.todosnew);

		$http.delete('/todos/' + id)
			.success(function(data) {
				// после удаления нужно обновить наш список
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};
}
